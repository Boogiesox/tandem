(function() {
    "use-strict"

    EVENT_LISTENERS = [
        {
            "element": "nav.mobile .hamburger",
            "interaction": "click",
            "handler": onMobileNavClick
        }
    ];

    EVENT_LISTENERS.forEach(function(listener) {
        document.querySelector(listener.element)
            .addEventListener(listener.interaction, listener.handler);
    });
    
    function onMobileNavClick(e) {
        document.querySelector("nav.mobile ul")
            .classList
            .toggle("show");
    }
})();